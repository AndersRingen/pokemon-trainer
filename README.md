# Pokémon Trainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.3.

This was Anders and Andreas' first ever project in Angular. The project is called Pokémon Trainer and it gives the user the possibility to login or create an account, and then catch pokemon to its pokemon collection. All the different catchable pokemon are displayed on the pokemon catalogue page, and the users collected pokemon will be displayed on the profile page. 

**Soon to be fullstack world champions**
