export class StorageUtil {

    /* A function that is saving a value to the session storage. */
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value))
    }

    /* A function that is saving a value to the Local storage. */
    public static storageSaveLocal<T>(key: string, value: T): void {
        localStorage.setItem(key, JSON.stringify(value))
    }
    
    /* A function that is reading a value from the session storage. */
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try{
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            } 
            return undefined;
        }
        catch(e){
            sessionStorage.removeItem(key);
            return undefined;
        }
    }
    /* A function that is reading a value from the Local storage. */
    public static storageReadLocal<T>(key: string): T | undefined {
        const storedValue = localStorage.getItem(key);
        try{
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            } 
            return undefined;
        }
        catch(e){
            localStorage.removeItem(key);
            return undefined;
        }
    }

    /**
     * This function deletes a key from the session storage.
     * @param {string} key - The key to store the data under.
     */
    public static storageDelete<T>(key: string): void{
        sessionStorage.removeItem(key);
    }

    /**
     * This function deletes a local storage item by key.
     * @param {string} key - The key to store the data under.
     */
    public static storageDeleteLocal<T>(key: string): void{
        localStorage.removeItem(key);
    }
}