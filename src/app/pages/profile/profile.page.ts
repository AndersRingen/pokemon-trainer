import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage/storage.utils';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {

  get onCat(): boolean {
    return false;
  }

  /**
   * If there is no user, throw an error. Otherwise, return the user
   * @returns The trainer object.
   */
  get trainer(): Trainer{
    if (!this.userService.user) {
      throw new Error("addToCollection: There is no trainer.")
    }
    return this.userService.user;
  }

  
  /**
   * "If the user has a pokemon collection, return a list of Pokemon objects that correspond to the
   * names in the user's collection."
   * The function is a getter, which means that it can be called like a property. 
   * @returns An array of Pokemon objects.
   */
  get collection(): Pokemon[] {
    let nameList = this.userService.user?.pokemon!;
    let pokemonList: Pokemon[] = [];
    for (let i = 0; i < nameList.length; i++) {
      pokemonList.push(this.pokemonCatalogueService.pokemonByName(nameList[i])!)
    }
    return pokemonList
  }

  constructor(
    private readonly userService : UserService,
    private readonly pokemonCatalogueService : PokemonCatalogueService,
  ) { }

  /**
   * If there is no pokemon in the local storage, then find all pokemon. If there is pokemon in the
   * local storage, then set the pokemon to the local storage.
   */
  ngOnInit(): void {
    if (!StorageUtil.storageRead(StorageKeys.Pokemon)) {
      this.pokemonCatalogueService.findAllPokemon();
    }else {
      this.pokemonCatalogueService.setPokemon(StorageUtil.storageRead(StorageKeys.Pokemon));
    }
  }
}
