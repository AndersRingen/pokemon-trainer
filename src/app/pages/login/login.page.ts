import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage {

  constructor(
    private readonly router: Router,
    private readonly userService : UserService
  ) { }

  /**
   * When the user clicks the login button, the router navigates to the pokemon page.
   */
  handleLogin(): void {
    this.router.navigateByUrl("/pokemon")
  }
  /**
   * If the user is logged in, then call the handleLogin function.
   */
  ngOnInit(): void {
    if(this.userService.user){
      this.handleLogin()
    }
  }
}
