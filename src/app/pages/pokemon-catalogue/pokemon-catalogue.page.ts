import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { StorageUtil } from 'src/app/utils/storage/storage.utils';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  /**
   * The getter function returns the value of the pokemon property of the pokemonCatalogueService
   * object.
   * @returns The pokemon array from the pokemonCatalogueService.
   */
  get pokemon(): Pokemon[]{
    return this.pokemonCatalogueService.pokemon;
  }

  /**
   * If the loading property of the pokemonCatalogueService is true, then return true, otherwise return
   * false.
   * @returns The loading property of the pokemonCatalogueService.
   */
  get loading(): boolean{
    return this.pokemonCatalogueService.loading;
  }

  /**
   * It returns the error message from the service.
   * @returns The error message from the service.
   */
  get error(): string {
    return this.pokemonCatalogueService.error;
  }

  get onCat(): boolean {
    return true;
  }
  

  constructor(
    private readonly pokemonCatalogueService : PokemonCatalogueService
  ) { }

  /**
   * If there is no pokemon in the local storage, then find all pokemon. If there is pokemon in the
   * local storage, then set the pokemon to the local storage.
   */
  ngOnInit(): void {
    if (!StorageUtil.storageRead(StorageKeys.Pokemon)) {
      this.pokemonCatalogueService.findAllPokemon();
    }else {
      this.pokemonCatalogueService.setPokemon(StorageUtil.storageRead(StorageKeys.Pokemon));
    }
  }
}
