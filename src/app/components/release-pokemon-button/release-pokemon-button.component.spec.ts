import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleasePokemonButtonComponent } from './release-pokemon-button.component';

describe('ReleasePokemonButtonComponent', () => {
  let component: ReleasePokemonButtonComponent;
  let fixture: ComponentFixture<ReleasePokemonButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReleasePokemonButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReleasePokemonButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
