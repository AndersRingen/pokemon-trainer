import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectionService } from 'src/app/services/collection.service';

@Component({
  selector: 'app-release-pokemon-button',
  templateUrl: './release-pokemon-button.component.html',
  styleUrls: ['./release-pokemon-button.component.css']
})
export class ReleasePokemonButtonComponent implements OnInit {

  @Input() pokemonId: number = 10871623548;
 
  constructor(
    private readonly collectionService: CollectionService
  ) { }

  ngOnInit(): void {

  }

  /**
   * When the user clicks the release button, the function will call the removeFromCollection function
   * in the collection service, which will send a request to the server to remove the pokemon from the
   * user's collection
   */
  onClickRelease(): void{
    this.collectionService.removeFromCollection(this.pokemonId)
    .subscribe({
      next: (user: Trainer) => {
        console.log("Removed pokémon!");
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
      }
    });
  }
}
