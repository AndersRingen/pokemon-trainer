import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectionService } from 'src/app/services/collection.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {

  public isCollected: boolean = false;

  @Input() pokemonId: number = 10871623548;

  /**
   * If the collectionService is loading, then return true, otherwise return false.
   * @returns The loading property of the collectionService.
   */
  get loading(): boolean {
    return this.collectionService.loading;
  }

  constructor(
    private readonly userService: UserService,
    private readonly collectionService: CollectionService
  ) { }

  /**
   * The isCollected variable is also used in the ngOnInit() function to determine whether the button
   * should be displayed.
   */
  ngOnInit(): void {
    this.isCollected = this.userService.inCollection(this.pokemonId);
  }

  /**
   * The function calls the addToCollection method in the collectionService, which returns an
   * observable. The observable is subscribed to, and the next function is called if the observable
   * returns a value. The next function sets the isCollected variable to the value returned by the
   * inCollection method in the userService. The error function is called if the observable returns an
   * error. The error function logs the error message to the console.
   * </code>
   */
  onCatchClick(): void {
    this.collectionService.addToCollection(this.pokemonId)
    .subscribe({
      next: (user: Trainer) => {
        this.isCollected = this.userService.inCollection(this.pokemonId)
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
      }
    })
  }
}
