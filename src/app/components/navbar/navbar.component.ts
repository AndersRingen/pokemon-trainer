import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Trainer } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage/storage.utils';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  get user(): Trainer | undefined{
    return this.userService.user
  }

  constructor(
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
  }

  /**
   * The function deletes the user from the userService and deletes the user and pokemon from the local
   * storage
   */
  logout():void{
    this.userService.user=undefined
    StorageUtil.storageDeleteLocal(StorageKeys.User)
    StorageUtil.storageDelete(StorageKeys.Pokemon)
  }

}
