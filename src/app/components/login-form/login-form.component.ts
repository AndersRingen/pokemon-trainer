import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService
    ) { }
  
  /**
   * The loginSubmit function takes a loginForm as an argument, and then it takes the username from the
   * loginForm, and then it calls the loginService.login function, and then it subscribes to the
   * loginService.login function, and then it emits the login event.
   * @param {NgForm} loginForm - NgForm - this is the form that was submitted
   */
  public loginSubmit(loginForm: NgForm):  void {

    const { username } = loginForm.value;

    this.loginService.login(username)
     .subscribe({
      next: (user: Trainer) => {
        this.userService.user = user
        this.login.emit();
      },
      error: () => {

      }
     })
  }
}
