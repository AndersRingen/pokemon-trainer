/* Defining the structure of the Pokemon object. */
export interface Pokemon {
    id: number;
    name: string;  
    url: string;
}