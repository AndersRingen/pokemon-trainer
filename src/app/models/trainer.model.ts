/* Defining the interface for the Trainer object. */
export interface Trainer {
    id: number;
    username: string;
    pokemon: string[];
}