import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { LoginPage } from "./pages/login/login.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { ProfilePage } from "./pages/profile/profile.page";

/* Defining the routes for the application. */
const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login" // here we are using the slash since we are redirecting and not declaring the path
    },
    {
        path: "login",
        component: LoginPage,
    },
    {
        path: "pokemon",
        component: PokemonCataloguePage,
        canActivate: [ AuthGuard ]
    },
    {
        path: "profile",
        component: ProfilePage,
        canActivate: [ AuthGuard ]
    }
]

/* Importing the RouterModule and exporting it. */
@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], // Always used to import a module
    exports: [
        RouterModule
    ] // Always used to expose the module and its features
})
export class AppRoutingModule {

}