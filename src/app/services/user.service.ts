import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage/storage.utils';
import { PokemonCatalogueService } from './pokemon-catalogue.service';

@Injectable({
  providedIn: 'root'
})
// keeping track of the user currently logged in
export class UserService {

  private _user?: Trainer;

  public get user(): Trainer | undefined {
    return this._user;
  }

  set user(user: Trainer | undefined){
    StorageUtil.storageSaveLocal(StorageKeys.User, user!)
    this._user = user;
  }

  constructor(
    private readonly pokemonService: PokemonCatalogueService
  ) { 
    this._user = StorageUtil.storageReadLocal<Trainer>(StorageKeys.User)
  }

  /**
   * If the user is logged in, return true if the user's pokemon collection contains the pokemon with
   * the given id
   * @param {number} pokemonId - number - The id of the pokemon you want to check if the user has in
   * their collection.
   * @returns A boolean value.
   */
  public inCollection(pokemonId: number): boolean {
    if (this._user) {
      return Boolean(this.user?.pokemon.find((pokemon: string) => pokemon ==  this.pokemonService.pokemonById(pokemonId)!.name));
    }
    return false;
  }


  /**
   * If the user exists, filter out the pokemon from the user's collection
   * @param {number} pokemonId - number - The id of the pokemon to remove from the collection
   */
  public removeFromCollection(pokemonId: number): void {
    if (this._user) {
      this._user.pokemon = this._user.pokemon.filter((pokemon: string) => pokemon !== this.pokemonService.pokemonById(pokemonId)?.name);
    }
  }
}
