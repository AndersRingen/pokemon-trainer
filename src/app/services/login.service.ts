import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap} from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainer, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency Injection
  constructor(private readonly http: HttpClient) { }

  /**
   * If the username exists, return the trainer. If it doesn't, create a new trainer and return that
   * @param {string} username - string - the username of the trainer
   * @returns Observable<Trainer>
   */
  public login(username: string): Observable<Trainer>{
    return this.checkUsername(username)
    .pipe(
      switchMap((trainer: Trainer | undefined) => {
        if (trainer == undefined) {
          return this.createTrainer(username)
        }
        return of(trainer);
      })
    )
  }
  
  /**
   * It takes a username as a parameter, makes a GET request to the API, and returns the last item in
   * the response array
   * @param {string} username - string - the username to check
   * @returns Observable<Trainer | undefined>
   */
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainer}?username=${username}`)
    .pipe(
      // RxJS
      map((response: Trainer[]) => response.pop())
    )
  }

  /**
   * It creates a new trainer object, sets the headers, and then sends a POST request to the API
   * @param {string} username - string - The username of the trainer to create.
   * @returns The trainer object
   */
  private createTrainer(username: string): Observable<Trainer>{
    const trainer = {
      username,
      pokemon: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey// her har api
    });
    return this.http.post<Trainer>(apiTrainer, trainer, {
      headers
    })
  }
}
