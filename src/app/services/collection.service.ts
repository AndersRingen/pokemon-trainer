import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage/storage.utils';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

const { apiKey, apiTrainer } = environment

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService
  ) { }


  /**
   * This function removes a pokemon from the user's collection
   * @param {number} pokemonId - number - The id of the pokemon you want to remove from the collection.
   * @returns The user is being returned.
   */
  public removeFromCollection(pokemonId: number): any{

    if (!this.userService.user) {
      throw new Error("addToCollection: There is no trainer.")
    }

    const user: Trainer = this.userService.user;
    this.userService.removeFromCollection(pokemonId); 
    this.userService.user = user;

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })
    return this.http.patch<Trainer>(`${apiTrainer}/${user.id}`, {
      pokemon: user.pokemon
    },{
      headers
    })
  }












  public addToCollection(pokemonId: number): Observable<Trainer>{
    if (!this.userService.user) {
      throw new Error("addToCollection: There is no trainer.")
    }

    const user: Trainer = this.userService.user;

    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId)
    
    if(!pokemon){
      throw new Error("addToCollection: No Pokemon with id: " + pokemonId)
    }

    if (this.userService.inCollection(pokemonId)) {
      throw new Error("addToCollection: This pokémon is already in your collection.")
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    this._loading = true;

    return this.http.patch<Trainer>(`${apiTrainer}/${user.id}`, {
      pokemon: [...user.pokemon, pokemon.name]
    },{
      headers
    })
    .pipe(
      tap((updatedUser: Trainer) => {
        this.userService.user = updatedUser;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
