import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage/storage.utils';


const { apiPokemon } = environment
@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemon: Pokemon[] = [];
  private _error: string = "";
  private _loading : boolean = false

  get pokemon(): Pokemon[] {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public setPokemon(pokemon: any): void {
    this._pokemon = pokemon;
  }

  /**
   * We're using the `http` service to make a `GET` request to the `apiPokemon` endpoint. We're using
   * the `pipe` method to chain together a `finalize` operator and a `subscribe` method. The `finalize`
   * operator is used to set the `_loading` variable to `false` and save the `_pokemon` variable to
   * local storage. The `subscribe` method is used to handle the response from the `http` service. If
   * the response is successful, we're looping through the results and adding an `id` property to each
   * result. We're also adding a `url` property to each result that points to the image of the pokemon.
   * If the response is unsuccessful, we're setting the `_error` variable to the error message
   */
  public findAllPokemon(): void {
    this._loading = true;
    this.http.get<any>(apiPokemon)
    .pipe(
      finalize(() => {
        this._loading = false
        StorageUtil.storageSave(StorageKeys.Pokemon, this._pokemon)
      }))
     .subscribe({
        next: (pokemon: any) => {
          for (let i = 0; i < pokemon.results.length; i++) {
            let oink = pokemon.results[i].url.split("/")
            pokemon.results[i].url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${oink[oink.length-2]}.png`;
            pokemon.results[i].id = i;
          }
          this._pokemon = pokemon.results;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message
        }
     })
  }


  /**
   * Find the pokemon in the array that has the same id as the id passed in as a parameter.
   * @param {number} id - number - The id of the pokemon we want to find
   * @returns The pokemon with the id that matches the id passed in.
   */
  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemon.find((pokemon: Pokemon) => pokemon.id === id)
  }


  /**
   * Return the Pokemon object from the array of Pokemon objects that has a name property that matches
   * the name argument.
   * @param {string} name - string - this is the name of the pokemon we're looking for.
   * @returns The pokemon object with the name that matches the name passed in.
   */
  public pokemonByName(name: string): Pokemon | undefined {
    return this._pokemon.find((pokemon: Pokemon) => pokemon.name === name)
  }
}
